import { Employee } from "./employee.js";
class hourlyEmployee extends Employee{
    __wage;
    __hours;

    constructor(paramWage,paramHours)
    {
        super(paramWage,paramHours)
        this.__wage=paramWage;
        this.__hours=paramHours;
    }
}
export{hourlyEmployee}