class Employee{
    __firstName;
    __lastName;
    __socialSecurityNumber;

    constructor(paramName,paramLastname,paramSocial)
    {
        this.__firstName=paramName;
        this.__lastName=paramLastname;
        this.__socialSecurityNumber=paramSocial;
    }

}
export{Employee}