import { commissionEmployee } from "./commissionEmployee.js";
class basePlusCommissionEmployee extends commissionEmployee{
    __baseSalary;

    constructor(paramBaseSalary)
    {
        super(paramBaseSalary);
        this.__baseSalary=paramBaseSalary;
    }
}
export{basePlusCommissionEmployee}