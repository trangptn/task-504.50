import { Employee } from "./employee.js";
class commissionEmployee extends Employee{
    __grossSales;
    __commissionRate

    constructor(paramSale,paramRate)
    {
        super(paramSale,paramRate);
        this.__grossSales=paramSale;
        this.__commissionRate=paramRate;

    }
}
export{commissionEmployee}