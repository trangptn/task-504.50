import { Employee } from "./employee.js";
import { commissionEmployee } from "./commissionEmployee.js";
import { hourlyEmployee } from "./hourlyEmployee.js";
import { salariedEmployee } from "./salariedEmployee.js";
import { basePlusCommissionEmployee } from "./basePlusCommissionEmployee.js";

var e1= new Employee("john","Mike","abc");
console.log(e1.__firstName);
console.log(e1.__lastName);
console.log(e1.__socialSecurityNumber);

var cE1= new commissionEmployee(200000,500000);
console.log(cE1.__commissionRate);
console.log(cE1.__grossSales);

var hE1=new hourlyEmployee(12,6);
console.log(hE1.__hours);
console.log(hE1.__wage);

var sE1=new salariedEmployee(5000000);
console.log(sE1.__weeklySalary);

var bpcE= new basePlusCommissionEmployee(3000000);
console.log(bpcE.__baseSalary);

console.log(e1 instanceof Employee);
console.log(e1 instanceof commissionEmployee);
console.log(e1 instanceof hourlyEmployee);
console.log(e1 instanceof salariedEmployee);
console.log(e1 instanceof basePlusCommissionEmployee);

console.log(cE1 instanceof Employee);
console.log(cE1 instanceof commissionEmployee);
console.log(cE1 instanceof hourlyEmployee);
console.log(cE1 instanceof salariedEmployee);
console.log(cE1 instanceof basePlusCommissionEmployee);

console.log(hE1 instanceof Employee);
console.log(hE1 instanceof commissionEmployee);
console.log(hE1 instanceof hourlyEmployee);
console.log(hE1 instanceof salariedEmployee);
console.log(hE1 instanceof basePlusCommissionEmployee);

console.log(sE1 instanceof Employee);
console.log(sE1 instanceof commissionEmployee);
console.log(sE1 instanceof hourlyEmployee);
console.log(sE1 instanceof salariedEmployee);
console.log(sE1 instanceof basePlusCommissionEmployee);

console.log(bpcE instanceof Employee);
console.log(bpcE instanceof commissionEmployee);
console.log(bpcE instanceof hourlyEmployee);
console.log(bpcE instanceof salariedEmployee);
console.log(bpcE instanceof basePlusCommissionEmployee);
